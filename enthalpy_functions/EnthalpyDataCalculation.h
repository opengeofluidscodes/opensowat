// we write the properties t, p, wt% NaCl and specific enthalpy as tab-limited files
// these can be imported into Excel and the like and the relevant colums can be used

namespace csp
{
  // 1. Enthalpies on phase diagram elements
  
  // Specific enthalpies on the V, L, and H lines of the VLH surface; note that these are polybaric!
  void VLH_VaporLineEnthalpyToFile();
  void VLH_LiquidLineEnthalpyToFile();
  void VLH_HaliteLineEnthalpyToFile();
  
  // Isothermal tie-lines on the VLH surface
  void VLH_IsothermalTieLinesEnthalpyToFile(       double tmin = 50., 
                                                   double tmax = 800.,
                                                   double dt   = 50.);

  // Isotherms and isobars on the halite-saturated vapor surface 
  void HaliteSaturatedVaporEnthalpyIsothermsToFile(double tmin = 50., 
                                                   double tmax = 800.,
                                                   double dt   = 50.);
  void HaliteSaturatedVaporEnthalpyIsobarsToFile(  double pmin = 100., 
                                                   double pmax = 300.,
                                                   double dp   = 100.);

  // Isotherms and isobars on the halite liquidus surface (i.e., these refer to the halite-saturated liquid)
  void HaliteLiquidusEnthalpyIsothermsToFile(      double tmin = 50., 
                                                   double tmax = 1000.,
                                                   double dt   = 50.);
  void HaliteLiquidusEnthalpyIsobarsToFile(        double pmin = 100., 
                                                   double pmax = 2200.,
                                                   double dp   = 99.99999);

  // Isotherms and isobars for halite
  void HaliteEnthalpyIsothermsToFile(              double tmin = 50., 
                                                   double tmax = 1000.,
                                                   double dt   = 50.);
  void HaliteEnthalpyIsobarsToFile(                double pmin = 100., 
                                                   double pmax = 2200.,
                                                   double dp   = 100.);

  // Enthalpy on the critical curve
  void CriticalCurveEnthalpyToFile();

  // Pure water isotherms and isobars
  void WaterEnthalpyIsothermsToFile(               double tmin = 50., 
                                                   double tmax = 1000.,
                                                   double dt   = 49.99999);
  void WaterEnthalpyIsobarsToFile(                 double pmin = 100., 
                                                   double pmax = 2200.,
                                                   double dp   = 99.99999);

  // Pure water boiling curve(s) (V and L)
  void WaterBoilingCurveEnthalpyToFile();

  // Liquid envelope of the liquid + vapor region, isotherms and isobars
  void TwophaseLiquidEnthalpyIsothermsToFile(      double tmin = 50., 
                                                   double tmax = 1000.,
                                                   double dt   = 49.99999);
  void TwophaseLiquidEnthalpyIsobarsToFile(        double pmin = 100., 
                                                   double pmax = 2200.,
                                                   double dp   = 100.);

  // Vapor envelope of the liquid + vapor region, isotherms and isobars
  void TwophaseVaporEnthalpyIsothermsToFile(       double tmin = 50., 
                                                   double tmax = 1000.,
                                                   double dt   = 49.99999);
  void TwophaseVaporEnthalpyIsobarsToFile(         double pmin = 100., 
                                                   double pmax = 2200.,
                                                   double dp   = 100.);
  
  // 2. Functions that were rather used for visualization but may also be useful for other purposes

  // Isobaric tie-lines between V, L, and H; demarking the triangular intersection of the VLH
  // surface with an isobar cutting throu the P-H-X diagram
  void VLH_IsobaricTieLinesEnthalpyToFile(         double pmin = 100., 
                                                   double pmax = 300.,
                                                   double dp   = 100.,
                                                   bool   log  = false);

  // This creates isothermal surfaces over p and x
  void SinglePhaseIsothermsInHXP(                  const double tmin = 100.,
                                                   const double tmax = 1000.,
                                                   const double dt   = 100.,
                                                   const double pmax = 2200. );

  // Isoplethic countours of V+L region
  void TwophaseIsoplethsToFile(                    double wtmin = 5.0,
                                                   double wtmax = 95.0,
                                                   double dwt   = 5.0);

  // Isoplethic-iothermal curves in the single-phase region
  void SinglephaseIsoplethsIsothermsToFile(        double wtmin = 10.0,
                                                   double wtmax = 90.0,
                                                   double dwt   = 9.9999999,
                                                   double tmin  = 50.0,
                                                   double tmax  = 1000.0,
                                                   double dt    = 49.9999999,
                                                   double pmax  = 2200.);


  // 3. Other
  
  // experimental
  /* void IsenthalpsInTPX(                      const double hmin = 0.5e6, */
  /*                                            const double hmax = 3.5e6, */
  /*                                            const double dh   = 0.5e6, */
  /*                                            const double pmin = 5., */
  /*                                            const double pmax = 2200., */
  /*                                                  double dp   = 100.); */

  // auxilliary functions
  double TwophaseLiquidPressureFromXandT(double myx, double myt);
  double TwophaseVaporPressureFromXandT(double myx, double myt);
  double LiquidusPressureFromXandT(double myx, double myt, double mypmax);
  double LiquidusTemperatureFromXandP(const double& myx, const double& myp);
  double GetDP( const double& p );
}
