#include "CSP_definitions.h"
#include "CSP_ErrorHandler.h"
#include <cstdio>
#include "tdlib.h"
#include "EnthalpyDataCalculation.h"
#include "HaliteLiquidus.h"
#include "Water.h"
#include "tdlib.h"

using namespace std;

namespace csp{
  //GlobalVariableDefinitions
  ErrorHandler skm_err;
  csp_float global_time = 0.0;
  csp::String global_physvar_textfile;
}
using namespace csp;

int main(){

   // IsenthalpsInTPX( 0.5e6,3.0e6, 0.5e6,1900.,2200.,100.);

  // double t,p;
  // Water water(t,p);
  // t = GetTempInC();
  // p = GetPressInBar();
  // cout << water.Enthalpy() << endl;
  // HaliteLiquidus liquidus(t,p);
  // t = 500.;
  // p = 678.;
  // double x = liquidus.Composition();
  // cout << LiquidusPressureFromXandT(x, t, 2200.) << endl;;

  //  SinglePhaseIsothermsInHXP( 100., 900., 100., 2200. );
  // SinglephaseIsoplethsIsothermsToFile();
  // TwophaseIsoplethsToFile();
  // VLH_VaporLineEnthalpyToFile();
  // VLH_LiquidLineEnthalpyToFile();
  // VLH_HaliteLineEnthalpyToFile();
  // VLH_IsothermalTieLinesEnthalpyToFile(50.,800.,50.);
  // VLH_IsobaricTieLinesEnthalpyToFile(50.,51.,100.,true);

  // HaliteSaturatedVaporEnthalpyIsothermsToFile(50.,800.,50.);
  // HaliteSaturatedVaporEnthalpyIsobarsToFile();
  // HaliteLiquidusEnthalpyIsothermsToFile(50.,800.,50.);
  //HaliteLiquidusEnthalpyIsobarsToFile(100.,2000.,99.99999);

  // HaliteEnthalpyIsothermsToFile(50.,800.,50.);
  // HaliteEnthalpyIsobarsToFile(100.,2000.,100.);

  // CriticalCurveEnthalpyToFile();

  // WaterBoilingCurveEnthalpyToFile();
  // WaterEnthalpyIsothermsToFile(50.,800.,50.);
  // WaterEnthalpyIsobarsToFile(100.,2000.,100.);

  //TwophaseLiquidEnthalpyIsothermsToFile(50.,1000.,50.);
  // TwophaseLiquidEnthalpyIsobarsToFile(50.,51.,100.);

  //TwophaseVaporEnthalpyIsothermsToFile(50.,1000.,50.);
  // TwophaseVaporEnthalpyIsobarsToFile(50.,51.,100.);
}
