#include <cstdlib>

#include "EnthalpyDataCalculation.h"

#include "Brine.h"
#include "CriticalPointH2O.h"
#include "CriticalCurve.h"
#include "Halite.h"
#include "HaliteLiquidus.h"
#include "NaClMeltingCurve.h"
#include "NaClSaturatedVapor.h"
#include "NaClSublimationCurve.h"
#include "TriplePointNaCl.h"
#include "TwophaseLiquid.h"
#include "TwophaseVapor.h"
#include "ThreephaseHLV.h"
#include "Water.h"
#include "steam4.h"

using namespace std;
using namespace csp;

namespace csp
{
  // adapted to Celsisus, Pa, mass fraction, not yet tested
  void VLH_VaporLineEnthalpyToFile()
  {
    // tested and ok'd 30.08.2013, TD
    double             t(25.);
    double             p(100.e5);
    double             x(0.1);
    Brine              brine(t,p,x);
    NaClSaturatedVapor naclsatvap(t,p);
    ThreephaseHLV      hlv(t);
    TriplePointNaCl    tp_nacl;

    ofstream ofile("enthalpy_vlh_v.dat",ios::out);
    ofile << "vlh_v_t\tvlh_v_p\tvlh_v_wt\tvlh_v_h\n";
    for(t = 1.0; t <= 800.; t += 1.)
      {
        p = hlv.Pressure()*0.999999999; // because of rare erratic non-functioning of naclsatvap.MassFractionNaCl() p_vlh
        x = naclsatvap.MassFractionNaCl();
        ofile << t << "\t" << p << "\t" << 100.*x << "\t" << brine.Enthalpy() << endl;
      }
    // Now a workaround for the triple point region
    // If one would use tpx values directly, result would be the liquid enthalpy
    // Therefore, use heat capacity of 4000 (valid here) and extrapolate
    double h  = brine.Enthalpy();
    double dh = 4000.*(tp_nacl.Temperature()-t);
    t = tp_nacl.Temperature();
    p = tp_nacl.Pressure();
    x = 1.0;
    ofile << t << "\t" << p << "\t" << 100.*x) << "\t" << h+dh << endl;

  cout << "done VLH_V\n";  
  ofile.close();
}

// adapted to Celsisus, Pa, mass fraction, not yet tested
void VLH_LiquidLineEnthalpyToFile()
{
  // tested and ok'd 30.08.2013, TD
  double             t(25.);
  double             p(100.e5);
  double             x(0.1);
  Brine              brine(t,p,x);
  HaliteLiquidus     liquidus(t,p);
  ThreephaseHLV      hlv(t);
  TriplePointNaCl    tp_nacl;

  ofstream ofile("enthalpy_vlh_l.dat",ios::out);
  ofile << "vlh_l_t\tvlh_l_p\tvlh_l_wt\tvlh_l_h\n";
  for(t = 1.0; t <= 800.; t += 1.)
    {
      p = hlv.Pressure();
      if(p < 1.01325e5) p = 1.01325e5; // atmospheric pressure as minimum; not in principle required but H2O-NaCl not tested for those lower p
      x = liquidus.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
    }
  t = tp_nacl.Temperature();
  p = tp_nacl.Pressure();
  x = 1.0;
  ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
    
  cout << "done VLH_L\n";  
  ofile.close();
}


// adapted to Celsisus, Pa, mass fraction, not yet tested
void VLH_HaliteLineEnthalpyToFile()
{
  // tested and ok'd 30.08.2013, TD
  double             t(25.);
  double             p(100.e5);
  Halite             halite(t,p);
  ThreephaseHLV      hlv(t);
  TriplePointNaCl    tp_nacl;

  ofstream ofile("enthalpy_vlh_h.dat",ios::out);
  ofile << "vlh_h_t\tvlh_h_p\tvlh_h_wt\tvlh_h_h\n";
  for(t = 1; t <=800.; t+=1.)
    {
      p = hlv.Pressure();
      ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
    }
  t = tp_nacl.Temperature();
  p = tp_nacl.Pressure();
  ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;

  cout << "done VLH_H\n";  
  ofile.close();
}


// adapted to Celsisus, Pa, mass fraction, not yet tested
void VLH_IsothermalTieLinesEnthalpyToFile(double tmin = 50., 
                                          double tmax = 800., // 1000 makes no sense here
                                          double dt   = 50.)
{
  double             t(25.);
  double             p(100.e5);
  double             x(0.1);
  double             pmax(0.0);

  Brine              brine(t,p,x);
  HaliteLiquidus     liquidus(t,p);
  NaClSaturatedVapor naclsatvap(t,p);
  Halite             halite(t,p);
  ThreephaseHLV      hlv(t);

  ofstream ofile("enthalpy_vlh_tielines_isothermal.dat",ios::out);
  ofile << "vlh_t\tvlh_p\tvlh_wt\tvlh_h\n";
  for(t = tmin; t <= tmax; t += dt)
    {
      pmax   = hlv.Pressure();
      p      = pmax*0.999999999; // because of rare erratic non-functioning of naclsatvap.MassFractionNaCl() p_vlh
      x      = naclsatvap.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
      p      = pmax;
      x      = liquidus.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
      ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy()<< endl << endl;
    }
  ofile.close();
  cout << "done VLH_tielines_isothermal\n";  
}


void SinglePhaseIsothermsInHXP(                  const double tmin = 100.,
                                                 const double tmax = 1000.,
                                                 const double dt   = 100.,
                                                 const double pmax = 2200.e5 )
{
  double              t, p, ph2o, x, wt, pmin, pcrit, dp, wtmax;
  Water               water(t,p);
  Brine               brine(t,p,x);
  TwophaseLiquid      twophase_l(t,p);
  TwophaseVapor       twophase_v(t,p);
  ThreephaseHLV       vlh(t);
  CriticalPointH2O    cp_h2o;
  HaliteLiquidus      liquidus(t,p);
  NaClSaturatedVapor  naclsatvap(t,p);
  CriticalCurve       critcurve(t);
    
  for(t = tmin; t <= tmax; t+= dt)
    {
      cout << "doing t = " << t << "...\n";
      char ofilename[60];
      sprintf(ofilename,"%f%s",t,"c.dat");
      ofstream ofile(ofilename);
      ofile << "t\tp\twt\th\n";
        
      if(t < cp_h2o.Temperature())
        {
          //            cout << "pmax is ... " << pmax << endl;
          // first do the horizontal mesh lines
          dp = GetDP(pmax);
          for( p = pmax; p > water.SaturationPressure(); p -= dp )
            {
              //                cout << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl;
              wt = 0.0; ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl;
              wtmax = liquidus.MassFractionNaCl()*100.;
              for(wt = 5.0; wt < wtmax; wt += 5.)
                {
                  x = wt*0.01;
                  ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                }
              wt = wtmax; x = liquidus.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
              ofile << endl;
            }
          p = vlh.Pressure(); wtmax = liquidus.MassFractionNaCl()*100.;
          for(wt = 0.0; wt < wtmax; wt += 5.)
            {
              x = wt*100.;
              p = TwophaseLiquidPressureFromXandT(x,t);
              ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
            }
          wt = wtmax; x = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
          ofile << endl;

          //then the vertical ones
          wt = 0.0;
          p = ph2o = water.SaturationPressure();
          ofile << t << "\t" << p << "\t" << wt << "\t" << water.LiquidSaturationEnthalpyForP() << endl; 
          for( p = ph2o+5.e5; p < pmax; p += dp)
            {
              dp = GetDP(p);
              //                cout << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl;
              ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl; 
            }
          p = pmax;
          ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl; 
          ofile << endl;

          p = vlh.Pressure(); double xmax = liquidus.MassFractionNaCl(); 
          for(wt = 5.0; wt < xmax*100.; wt += 5.0)
            {
              // check if in LH field
              x = wt*0.01;
              if(x > xmax) continue;
              // now check if liquidus is intersected; if so, obtain intersection 
              double mypmax = pmax;
              p = pmax; xmax = liquidus.MassFractionNaCl();
              if( x > xmax) mypmax = LiquidusPressureFromXandT(x,t,2200.);
              //find pressure on liquid surface
              pmin = TwophaseLiquidPressureFromXandT(x,t); 
              dp   = 5.e5;
              for( p = pmin; p < mypmax; p += dp ){
                dp = GetDP(p);
                ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
              }
              p = mypmax;
              ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
              //                cout << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
              ofile << endl;
            }
        }

      else // t>tcrit
        {
          // first do the horizontal mesh lines
          dp = GetDP(pmax);
          for( p = pmax; p > critcurve.Pressure(); p -= dp )
            {
              cout << "doning p = " << p << endl;
              wt = 0.0; ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl;
              wtmax = liquidus.MassFractionNaCl();
              for(wt = 5.0; wt < wtmax; wt += 5.)
                {
                  x = wt*0.01;
                  ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                }
              wt = wtmax; x = liquidus.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
              ofile << endl;
            }
          double newpmax = p; cout << "newpmax = " << newpmax << endl;
          for( p = newpmax; p > 0.; p -= dp )
            {
              if( p > vlh.Pressure() )
                {
                  // between water and twophase_v
                  double dwt = 10.*twophase_v.MassFractionNaCl(); // MassFraction*100 *0.1
                  wt = 0.0; ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl;
                  for(wt = dwt; wt < twophase_v.MassFractionNaCl()*100.; wt += dwt)
                    {
                      x = wt*0.01;
                      ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                    }
                  x = twophase_v.MassFractionNaCl(); wt = x *100.;
                  ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                  ofile << endl;
                  // between twophase_l and liquidus
                  x = twophase_l.MassFractionNaCl(); wt = x*100.; 
                  dwt = 0.05*( liquidus.MassFractionNaCl()*100. - wt );
                  cout << "wt = " << wt << endl;
                  cout << "dwt = " << dwt << endl;
                  //cout << "enter a char: "; char mychar; cin >> mychar;
                  for(wt = 100.*twophase_l.MassFractionNaCl(); wt < 100.*liquidus.MassFractionNaCl(); wt += dwt)
                    {
                      cout << "wt = " << wt << endl;
                      x = wt*0.01;
                      ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                    }
                  x = liquidus.MassFractionNaCl(); wt = 100.*x; 
                  ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                  ofile << endl;
                }
              else
                {
                  // between water and NaCl saturated vapor
                  double dwt = naclsatvap.MassFractionNaCl()*100.*0.1;
                  wt = 0.0; ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl;
                  for(wt = dwt; wt < naclsatvap.MassFractionNaCl()*100.; wt += dwt)
                    {
                      x = wt*0.01;
                      ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                    }
                  x = naclsatvap.MassFractionNaCl(); wt = x*100.; 
                  ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                  ofile << endl;
                }
            }
          //then the vertical ones
          wt = 0.0;
          dp = GetDP(100.e5);
          for( p = dp; p < pmax; p += dp)
            {
              dp = GetDP(p);
              //                cout << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl;
              ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl; 
            }
          p = pmax;
          ofile << t << "\t" << p << "\t" << wt << "\t" << water.Enthalpy() << endl; 
          ofile << endl;
            
          p = vlh.Pressure(); double xmax = liquidus.MassFractionNaCl(); 
          for(wt = 5.0; wt < xmax*100.; wt += 5.0)
            {
              // check if in LH field
              x = wt*0.01;
              if(x > xmax) continue;
              if(x < critcurve.MassFractionNaCl())
                {
                  pmin = TwophaseVaporPressureFromXandT(x,t); // theroretically (if dwt < 5), this could also end on NaCl saturated vapor, skipped it
                  dp = GetDP(pmin);
                  for(p = pmin; p < pmax; p +=dp)
                    {
                      dp = GetDP(p);
                      ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                    }
                  p = pmax;
                  ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                  ofile << endl;
                }
              else // x > xcrit
                {
                  // now check if liquidus is intersected; if so, obtain intersection 
                  double mypmax = pmax;
                  p = pmax; xmax = liquidus.MassfractionNaCl(); //****changing xmax -> double check if possible error *****
                  if( x > xmax) mypmax = LiquidusPressureFromXandT(x,t,2200.);
                  //find pressure on liquid surface
                  dp   = 5.e5;
                  pmin = TwophaseLiquidPressureFromXandT(x,t);
                  for( p = pmin; p < mypmax; p += dp )
                    {
                      dp = GetDP(p);
                      ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                    }
                  p = mypmax;
                  ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                  ofile << endl;
                }
            }
        }        
    }
}


 

void VLH_IsobaricTieLinesEnthalpyToFile(double pmin = 100.e5, 
                                        double pmax = 300.e5,
                                        double dp   = 100.e5,
                                        bool   log  = false)
{
  double             t(25.);
  double             p(100.e5);
  double             x(0.1);
  double             tmin(0.0);
  double             tmax(0.0);
  double             dt(0.0);
  double             xv(0.0), xl(0.0), dx(0.0), hv(0.0), hl(0.0), hh(0.0);
  double             logxv(0.0), logxl(0.0), logxh(0.0), logx(0.0);

  Brine              brine(t,p,x);
  HaliteLiquidus     liquidus(t,p);
  NaClSaturatedVapor naclsatvap(t,p);
  Halite             halite(t,p);
  ThreephaseHLV      hlv(t);

  ofstream ofile("enthalpy_vlh_tielines_isobaric.dat",ios::out);
  ofile << "vlh_t\tvlh_p\tvlh_wt\tvlh_h\n";
  for(p = pmin; p <= pmax; p += dp)
    {
      tmin = hlv.TfromP(p,450.)*1.000000001;
      tmax = hlv.TfromP(p,700.)*0.999999999;
        
      if(!log)
        {
          t = tmin;
          p     *= 0.999999999;
          x      = naclsatvap.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          p     /= 0.999999999;
          x      = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy()<< endl << endl;
            
          t = tmax;
          p     *= 0.999999999;
          x      = naclsatvap.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          p     /= 0.999999999;
          x      = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy()<< endl << endl;
        }
      else
        {
          t = tmin;
          p     *= 0.999999999;
          xv = x = naclsatvap.MassFractionNaCl();
          logxv  = log10(xv);
          hv     = brine.Enthalpy();
          p     /= 0.999999999;
          xl = x = liquidus.MassFractionNaCl();
          logxl  = log10(xl);
          hl     = brine.Enthalpy();
          hh     = halite.Enthalpy();
          logxh  = log10(1.0);
            
          dx = 0.01*(logxl-logxv);
          for(logx = logxv; logx < logxl; logx += dx)
            {
              x = pow(10.0,logx);
              ofile << t << "\t" << p << "\t" << x*100. << "\t" 
                    << hv+(x-xv)/(xl-xv)*(hl-hv) << endl;
            }
            
          dx = 0.01*(logxh-logxl);
          for(logx = logxl; logx < logxh; logx += dx)
            {
              x = pow(10.0,logx);
              ofile << t << "\t" << p << "\t" << x*100. << "\t" 
                    << hl+(x-xl)/(1.0-xl)*(hh-hl) << endl;
            }
          x = 1.0;
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << hh << endl;
            
          dx = 0.01*(logxh-logxv);
          for(logx = logxh-dx; logx > logxv; logx -= dx)
            {
              x = pow(10.0,logx);
              ofile << t << "\t" << p << "\t" << x*100. << "\t" 
                    << hh-(100.0-x*100.)/(100.0-xv*100.)*(hh-hv) << endl;
            }
          x = xv;
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << hv << endl;
          ofile << endl;
            
          t = tmax;
          p     *= 0.999999999;
          xv = x = naclsatvap.MassFractionNaCl();
          logxv  = log10(xv);
          hv     = brine.Enthalpy();
          p     /= 0.999999999;
          xl = x = liquidus.MassFractionNaCl();
          logxl  = log10(xl);
          hl     = brine.Enthalpy();
          hh     = halite.Enthalpy();
          logxh  = log10(1.0);
            
          dx = 0.01*(logxl-logxv);
          for(logx = logxv; logx < logxl; logx += dx)
            {
              x = pow(10.0,logx);
              ofile << t << "\t" << p << "\t" << x*100. << "\t" 
                    << hv+(x-xv)/(xl-xv)*(hl-hv) << endl;
            }
	    
          dx = 0.01*(logxh-logxl);
          for(logx = logxl; logx < logxh; logx += dx)
            {
              x = pow(10.0,logx);
              ofile << t << "\t" << p << "\t" << x*100. << "\t" 
                    << hl+(x-xl)/(1.0-xl)*(hh-hl) << endl;
            }
          x = 1.0;
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << hh << endl;
	    
          dx = 0.01*(logxh-logxv);
          for(logx = logxh-dx; logx > logxv; logx -= dx)
            {
              x = pow(10.0,logx);
              ofile << t << "\t" << p << "\t" << x*100. << "\t" 
                    << hh-(1.0-x)/(1.0-xv)*(hh-hv) << endl;
            }
          x = xv;
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << hv << endl;
          ofile << endl;
        }
      ofile.close();
      cout << "done VLH_tielines_isobaric\n";  
    }
}

void HaliteSaturatedVaporEnthalpyIsothermsToFile(double tmin = 50., 
                                                 double tmax = 800., // higher doesn't make sense (no halite above 800.7)
                                                 double dt   = 50.)
{
  double                t(25.); 
  double                p(100.e5); 
  double                x(0.1); 
  double                pmin(0.0); 
  double                pmax(1.0e5); 
  double                dp(0.01e5);
  Brine                 brine(t,p,x);
  NaClSaturatedVapor    naclsatvap(t,p);
  NaClSublimationCurve  naclsubl(t);
  ThreephaseHLV         hlv(t);
  TriplePointNaCl       tp_nacl;

  ofstream ofile("enthalpy_vh_isotherms.dat",ios::out);
  ofile << "vh_t\tvh_p\tvh_wt\tvh_h\n";
  for(t = tmin; t <= tmax; t += dt)
    {
      // double check if non-default tmax is used
      if(tmax > tp_nacl.Temperature()) continue;

      // notice that enthalpies below 1 bar are not really well-constrained by the model
      // so, this may need some reworking
      pmin = naclsubl.Pressure();
      pmax = hlv.Pressure();
      dp   = 0.02*(pmax-pmin);
      
      for(p = pmin; p <= pmax-dp; p+= dp)
        {
          x = naclsatvap.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
        }
      p    = pmax*0.99999999;
      x    = naclsatvap.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
      ofile << endl;
    }
  ofile.close();
  cout << "done VH_isotherms\n";  
}


void HaliteSaturatedVaporEnthalpyIsobarsToFile(double pmin = 100.e5, 
                                               double pmax = 300.e5,
                                               double dp   = 100.e5)
{
  double                t(25.); 
  double                p(100.e5); 
  double                x(0.1); 
  double                tmin(0.0); 
  double                tmax(1.0); 
  double                dt(0.01);
  Brine                 brine(t,p,x);
  NaClSaturatedVapor    naclsatvap(t,p);
  NaClSublimationCurve  naclsubl(t);
  ThreephaseHLV         hlv(t);

  ofstream ofile("enthalpy_vh_isobars.dat",ios::out);
  ofile << "vh_t\tvh_p\tvh_wt\tvh_h\n";
  for(p = pmin; p <= pmax; p += dp)
    {
      // here place some checks for too low pressure

      tmin = hlv.TfromP(p,450.)*1.000000001;
      tmax = hlv.TfromP(p,700.)*0.999999999;
      dt   = 1.0e-2*(tmax-tmin);

      for(t = tmin; t <= tmax-dt; t+= dt)
        {
          x = naclsatvap.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
        }
      t    = tmax;
      x    = naclsatvap.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
      ofile << endl;
    }
  ofile.close();
  cout << "done VH_isobars\n";  
}


void HaliteLiquidusEnthalpyIsothermsToFile(double tmin = 50., 
                                           double tmax = 1000.,
                                           double dt   = 50.)
{
  double                t(25.); 
  double                p(100.e5);
  double                x(0.1);
  double                pmin(0.0);
  double                pmax(1.0e5);
  double                dp(0.01e5);
  Brine                 brine(t,p,x);
  HaliteLiquidus        liquidus(t,p);
  NaClSublimationCurve  naclsubl(t);
  NaClMeltingCurve      naclmelt(t,p);
  ThreephaseHLV         hlv(t);
  TriplePointNaCl       tp_nacl;

  ofstream ofile("enthalpy_lh_isotherms.dat",ios::out);
  ofile << "lh_t\tlh_p\tlh_wt\tlh_h\n";
  pmax = 2200.e5; 
  for(t = tmin; t <= tmax; t += dt)
    {
      if(t > tp_nacl.Temperature())
        pmin = naclmelt.Pressure();
      else
        pmin = hlv.Pressure();
      if(pmin > pmax) continue; // t is too high, we'd be in the NaCl melt field
      dp = 0.01*(pmax-pmin); 

      p = pmin;
      ofile << t << "\t" << pmin << "\t" << x*100.;
      if(pmin < 1.0e5) pmin = 1.0e5;
      p = pmin;
      x = liquidus.MassFractionNaCl();
      ofile  << "\t" << brine.Enthalpy() << endl;

      for(p = pmin+dp; p <= pmax-dp; p+= dp)
        { 
          x = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
        }
      p = pmax;
      x = liquidus.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;

      ofile << endl;
    }
  ofile.close();
  cout << "Done LH isotherms ... \n";
}


void HaliteLiquidusEnthalpyIsobarsToFile(double pmin = 100.e5, 
                                         double pmax = 2200.e5,
                                         double dp   = 99.99999e5)
{
  double                t(25.);
  double                p(100.e5);
  double                x(0.1);
  double                tmin(0.0);
  double                tmax(1.0);
  double                dt(0.01);
  Brine                 brine(t,p,x);
  HaliteLiquidus        liquidus(t,p);
  ThreephaseHLV         hlv(t);
  NaClMeltingCurve      naclmelt(t,p);
  TriplePointNaCl       tp_nacl;
  
  ofstream ofile("enthalpy_lh_isobars.dat",ios::out);
  ofile << "lh_t\tlh_p\tlh_wt\tlh_h\n";
  for(p = pmin; p <= pmax; p += dp)
    {
      if(p < hlv.Pmax()) // isobar will be intersected by VLH surface -> high T and low T segments
        {
          tmax = hlv.TfromP(p,450.); // find intersection on the lowT side (maximum is near 600C, so 450 will force iteration to low T side)
          dt   = 1.0e-2 * (tmax-1.0);
      
          for(t = 1.0; t < tmax; t+= dt)
            {
              x = liquidus.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
            }
          t = tmax;
          x = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          ofile << "\n";

          tmin = hlv.TfromP(p,700.); // find intersection on high T side, same logic as above
          tmax = naclmelt.TmeltFromP();
          if(tmax > 1000.) tmax = 1000.; // should not be the case for valid P range (up to 5000.e5 Pa) but double-check

          dt = 0.01*(tmax-tmin);
          for(t = tmin; t <= tmax-dt; t+= dt)
            {
              x = liquidus.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
            }
          t = tmax;
          x = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          ofile << endl;
        }
      else
        {
          tmax = naclmelt.TmeltFromP();
          if(tmax > 1000.) tmax = 1000.;
          dt = 0.01*(tmax - 1.0);
          for(t = 1.0; t <= 1000.; t+= dt)
            {
              x = liquidus.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
            }
          ofile << endl;
        }
    }
  ofile.close();
  cout << "Done LH Isobars ...\n";
}


void HaliteEnthalpyIsothermsToFile(double tmin = 50., 
                                   double tmax = 1000.,
                                   double dt   = 50.)
{
  double           t(25.);
  double           p(100.e5);
  double           x(0.1);
  double           pmin(0.0);
  double           pmax(1.0e5);
  double           dp(0.01);
  Halite           halite(t,p);
  ThreephaseHLV    hlv(t);
  NaClMeltingCurve naclmelt(t,p);
  TriplePointNaCl  tp_nacl;

  ofstream ofile("enthalpy_h_isotherms.dat",ios::out);
  ofile << "h_t\th_p\th_wt\th_h\n";
  for(t = tmin; t <= tmax; t += dt)
    {
      if(t <= tp_nacl.Temperature())
        pmin = hlv.Pressure();
      else
        pmin = naclmelt.Pressure();
      pmax = 2200.e5;
      if(pmin > pmax) continue; // t too high, we'd be in the NaCl melt field! 

      dp = 0.01*(pmax-pmin); 
      for(p = pmin; p <= pmax-dp; p+= dp)
        { 
          ofile << t << "\t" << p << "\t" << XNaCl2Weight(1.) << "\t" << halite.Enthalpy() << endl;
        }
      p = pmax;
      ofile << t << "\t" << p << "\t" << XNaCl2Weight(1.) << "\t" << halite.Enthalpy() << endl;
      ofile << endl;
    }
  ofile.close();
  cout << "Done H isotherms ... \n";
}


void HaliteEnthalpyIsobarsToFile(double pmin = 100.e5, 
                                 double pmax = 2200.e5,
                                 double dp   = 100.e5)
{
  double           t(25.);
  double           p(100.e5);
  double           x(0.1);
  double           tmin(0.0);
  double           tmax(1.0);
  double           dt(0.01);
  Halite           halite(t,p);
  ThreephaseHLV    hlv(t);
  NaClMeltingCurve naclmelt(t,p);
  TriplePointNaCl  tp_nacl;
  
  ofstream ofile("enthalpy_h_isobars.dat",ios::out);
  ofile << "h_t\th_p\th_wt\th_h\n";
  for(p = pmin; p < pmax; p += dp)
    {
      if(p < hlv.Pmax())
        {     
          tmax = hlv.TfromP(p,450.);
          dt   = 1.0e-2*(tmax-1.0);
      
          for(t = 1.0; t <= tmax-dt; t+= dt)
            {
              ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
            }
          t = tmax;
          ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
          ofile << "\n";

          tmin = hlv.TfromP(p,700.);
          tmax = naclmelt.TmeltFromP();
          if(tmax > 1000.0) tmax = 1000.0;
          dt   = 0.01*(tmax-tmin);
          for(t = tmin; t <= tmax-dt; t+= dt)
            {
              ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
            }
          t = tmax;
          ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
          ofile << endl;
        }
      else
        {
          tmin = 1.0;
          tmax = naclmelt.TmeltFromP();
          dt   = 0.01*(tmax-tmin);
          for(t = 1.0; t <= tmax-dt; t += dt)
            ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
          t = tmax; 
          ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
          ofile << endl;
        }
    }
  p = pmax;
  tmin = 1.0;
  tmax = naclmelt.TmeltFromP();
  dt   = 0.01*(tmax-tmin);
  for(t = 1.0; t <= tmax-dt; t += dt)
    ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
  t = tmax; 
  ofile << t << "\t" << p << "\t100.0\t" << halite.Enthalpy() << endl;
  ofile.close();
  cout << "Done H Isobars ...\n";
}


void CriticalCurveEnthalpyToFile()
{
  double             t(25.);
  double             p(100.e5);
  double             x(0.1);
  double             tmin(0.0);
  double             tmax(0.0);
  double             dt(0.0);
  Brine              brine(t,p,x);
  CriticalCurve      critcurve(t);
  CriticalPointH2O   cp_h2o;

  ofstream ofile("enthalpy_crit.dat",ios::out);
  ofile << "crit_t\tcrit_p\tcrit_wt\tcrit_h\n";

  tmin = cp_h2o.Temperature();
  tmax = 400.;
  dt = 0.01*(tmax-tmin);
  for(t = tmin; t < tmax; t += dt)
    {
      p = critcurve.Pressure();
      x = critcurve.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
    }

  tmin = 400.;
  tmax = 1000.;
  dt = 0.01*(tmax-tmin);
  for(t = tmin+dt; t < tmax; t += dt)
    {
      p = critcurve.Pressure();
      x = critcurve.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
    }

  ofile.close();
  cout << "done critcurve_h\n";  
}


void WaterEnthalpyIsothermsToFile( double tmin = 50., 
                                   double tmax = 1000.,
                                   double dt   = 49.99999)
{
  double            t;
  double            p;
  double            pmin(1.0);
  double            pmax(2200.0);
  double            dp(1.0);
  Water             water(t,p);
  CriticalPointH2O  cp_h2o;

  ofstream ofile("enthalpy_water_isotherms.dat",ios::out);
  ofile << "water_t\twater_p\twater_wt\twater_h\n";

  for(t = tmin; t <= tmax; t += dt)
    {
      if(t < cp_h2o.Temperature())
        {
          if(water.SaturationPressure()*1.0e-5 < 1.0)
            { 
              // Saturation pressure below 1 bar -> will not be reached in
              // geological situations; hence plot only liquid branch
              pmin = 1.0;
              pmax = 2200;
              dp   = 0.01*(pmax-pmin);
              ofile << t << "\t" << water.SaturationPressure()*1.0e-5 << "\t0.0\t" 
                    << water.LiquidSaturationEnthalpyForT() << endl;
              for(p = pmin; p <= pmax-dp; p += dp)
                ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
              p = pmax;
              ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
              ofile << endl;
            }
          else
            {
              // Saturation pressure above 1 bar -> create plot data for both
              // vapor and liquid branches
              pmin = 1.0;
              pmax = water.SaturationPressure()*1.0e-5;
              dp   = 0.01*(pmax-pmin);
              for(p = pmin; p <= pmax-dp; p += dp)
                ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
              ofile << t << "\t" << pmax << "\t0.0\t" 
                    << water.VaporSaturationEnthalpyForT() << endl;
              ofile << endl;

              pmin = water.SaturationPressure()*1.0e-5;
              pmax = 2200.;
              dp   = 0.01*(pmax-pmin);
              ofile << t << "\t" << pmin << "\t0.0\t" << water.LiquidSaturationEnthalpyForT() << endl;
              for(p = pmin+dp; p <= pmax-dp; p += dp)
                ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
              p = pmax;
              ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
              ofile << endl;
            }
        }
      else
        {
          pmax = 2200.;
          dp = 0.01*(pmax-1.);
          for(p = 1.0; p <= pmax-dp; p += dp)
            ofile << t << "\t" << p << "\t" << XNaCl2Weight(0.0) << "\t" << water.Enthalpy() << endl;
          p = pmax;
          ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
          ofile << endl;
        }

    }
  ofile.close();
  cout << "done water isotherms_h\n";  
}


void WaterEnthalpyIsobarsToFile( double pmin = 100., 
                                 double pmax = 2200.,
                                 double dp   = 99.99999)
{
  double            t;
  double            p;
  double            tmin(1.0);
  double            tmax(1000.0);
  double            dt(1.0);
  Water             water(t,p);
  CriticalPointH2O  cp_h2o;

  ofstream ofile("enthalpy_water_isobars.dat",ios::out);
  ofile << "water_t\twater_p\twater_wt\twater_h\n";

  for(p = pmin; p <= pmax; p += dp)
    {
      if(p < cp_h2o.Pressure())
        {
          t = 300.; //dummy
          tmin = 1.0;
          tmax = water.SaturationTemperature();
          dt   = 1.0;
          for(t = tmin; t < tmax-dt; t += dt)
            ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
          ofile << tmax << "\t" << p << "\t0.0\t" 
                << water.LiquidSaturationEnthalpyForP() << endl;
          ofile << endl;
	  
          tmin = water.SaturationTemperature();
          tmax = 1000.;
          dt   = 1.0;
          ofile << tmin << "\t" << p << "\t0.0\t" << water.VaporSaturationEnthalpyForT() << endl;
          tmin = double(int(water.SaturationTemperature()+.01))+1.0;
          for(t = tmin; t <= tmax; t += dt)
            ofile << t << "\t" << p << "\t0.0\t" << water.Enthalpy() << endl;
          ofile << endl;
        }
      else
        {
          dt = 1.0;
          for(t = 1.0; t <= 1000.; t += dt)
            ofile << t << "\t" << p << "\t" << XNaCl2Weight(0.0) << "\t" << water.Enthalpy() << endl;
          ofile << endl;
        }
    }
  ofile.close();
  cout << "done water isobars_h\n";  
}


void WaterBoilingCurveEnthalpyToFile()
{
  double            t;
  double            p;
  Water             water(t,p);
  CriticalPointH2O  cp_h2o;

  ofstream ofile("enthalpy_boiling_curve.dat",ios::out);
  ofile << "water_t\twater_p\twater_wt\twater_h\n";  

  for(t = 1.0; t <= 373.0; t += 1.0)
    {
      ofile << t << "\t" << water.SaturationPressure()*1.0e-5 << "\t0.0\t" 
            << water.VaporSaturationEnthalpyForT() << endl;
    }
  ofile << cp_h2o.Temperature() << "\t" << cp_h2o.Pressure() << "\t0.0\t" 
        << cp_h2o.Enthalpy() << endl;  
  for(t = 373.0; t >= 1.0; t -= 1.0)
    {
      ofile << t << "\t" << water.SaturationPressure()*1.0e-5 << "\t0.0\t" 
            << water.LiquidSaturationEnthalpyForT() << endl;
    }

  ofile.close();
  cout << "done water boiling curve h\n";  
}



void TwophaseLiquidEnthalpyIsothermsToFile(      double tmin = 50., 
                                                 double tmax = 1000.,
                                                 double dt   = 49.99999)
{
  double            t;
  double            p;
  double            myp;
  double            x;
  double            pmin;
  double            pmax;
  double            dp;
  double            ph2o;
  double            dph2odt;
  Water             water(t,p);
  CriticalPointH2O  cp_h2o;
  HaliteLiquidus    liquidus(t,p);
  TwophaseLiquid    twophase_l(t,p,ph2o,dph2odt);
  Brine             brine(t,p,x);
  CriticalCurve     critcurve(t);
  ThreephaseHLV     hlv(t);
  TriplePointNaCl   tp_nacl;
  NaClBoilingCurve  naclboil(t,p);

  ofstream ofile("enthalpy_vl_l_isotherms.dat",ios::out);
  ofile << "vl_l_t\tvl_l_p\tvl_l_wt\tvl_l_h\n";

  for(t = tmin; t <= tmax; t+= dt)
    {
      if(t < cp_h2o.Temperature()) 
        {
          ph2o    = water.SaturationPressure()*1.0e-5;
          dph2odt = 1.0;
          pmax    = ph2o;
        }
      else 
        {
          pmax = critcurve.Pressure();
        }
      if(t > tp_nacl.Temperature())
        pmin = naclboil.Pressure();
      else
        pmin = hlv.Pressure();

      if(t < 400.0) dp = 0.01*(pmax-pmin);
      else dp   = 0.002*(pmax-pmin);

      p    = pmin;
      x    = liquidus.MassFractionNaCl();
      ofile << t << "\t" << p << "\t" << x*100.;
      myp  = p;
      if(p < 1.00001) p = 1.00001;
      ofile << "\t" << brine.Enthalpy() << endl;
      p    = myp; 

      for(p = pmin+dp; p < pmax-dp ; p+= dp)
        {
          x    = twophase_l.MassFractionNaCl();
          myp  = p;
          if(p <= 1.00001) p = 1.000001;
          ofile << t << "\t" << myp << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          p    = myp;
        }

      p = pmax;
      if(t < 373.976)
        {
          ofile << t << "\t" << p << "\t" << XNaCl2Weight(0.0) << "\t" << water.LiquidSaturationEnthalpyForT() << endl;
        }
      else
        {
          x = critcurve.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
        }
      ofile << endl;
    }
  ofile.close();
  cout << "done twophase liquid isotherms\n";
}


void TwophaseLiquidEnthalpyIsobarsToFile( double pmin = 100., 
                                          double pmax = 2200.,
                                          double dp   = 100.)
{
  double            t;
  double            p;
  double            x;
  double            tmin;
  double            tmax;
  double            t_vlh_low;
  double            t_vlh_high;
  double            dt;
  double            ph2o;
  double            dph2odt;
  Water             water(t,p);
  CriticalPointH2O  cp_h2o;
  HaliteLiquidus    liquidus(t,p);
  TwophaseLiquid    twophase_l(t,p,ph2o,dph2odt);
  Brine             brine(t,p,x);
  CriticalCurve     critcurve(t);
  ThreephaseHLV     hlv(t);

  ofstream ofile("enthalpy_vl_l_isobars.dat",ios::out);
  ofile << "vl_l_t\tvl_l_p\tvl_l_wt\tvl_l_h\n";

  for(p = pmin; p <= pmax; p += dp)
    {
      t = 1000.;
      if( p > critcurve.Pressure()) continue;
      t = 300.;
	
      if(p < hlv.Pmax())
        {
          t_vlh_low  = hlv.TfromP(p,450.)*0.999999;
          t_vlh_high = hlv.TfromP(p,700.)*1.000001;
      	  
          // low T segment
          if(p < cp_h2o.Pressure()) 
            {
              tmin    = water.SaturationTemperature();
              ofile << tmin << "\t" << p << "\t" << XNaCl2Weight(0.0) << "\t" 
                    << water.LiquidSaturationEnthalpyForP() << endl;
            }
          else
            {
              tmin = critcurve.TfromP(p);
              t    = tmin;
              x    = critcurve.MassFractionNaCl();
              ofile << tmin << "\t" << p << "\t" << x*100. << "\t" 
                    << brine.Enthalpy() << endl;
            }
          tmin = double(int(tmin+0.01))+1.0;
          tmax = double(int(t_vlh_low-0.01));

          for(t = tmin+0.01; t <= tmin+0.99; t += 0.01)
            {
              if(p < cp_h2o.Pressure())
                {
                  ph2o    = water.SaturationPressure()*1.0e-5;
                  dph2odt = 1.0;
                }
              x = twophase_l.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
            }
          for(t = tmin+1.0; t <= tmax; t += 1.0)
            {
              if(p < cp_h2o.Pressure())
                {
                  ph2o    = water.SaturationPressure()*1.0e-5;
                  dph2odt = 1.0;
                }
              x = twophase_l.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
            }
          t = t_vlh_low;
          x = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          ofile << endl;

          // high T segment
          t = t_vlh_high;
          x = liquidus.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;

          tmin = double(int(t_vlh_high+0.01))+1.0;
          for(t = tmin+1.0; t <= 1000.; t += 1.0)
            {
              x = twophase_l.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;	      
            }
          ofile << endl;
        }

      else
        {
          tmin = critcurve.TfromP(p);
          t    = tmin;
          x    = critcurve.MassFractionNaCl();
          ofile << tmin << "\t" << p << "\t" << x*100. << "\t" 
                << brine.Enthalpy() << endl;

          tmin = double(int(tmin+0.01))+1.0;
          for(t = tmin; t <= 1000.; t += 1.0)
            {
              x = twophase_l.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;	      
            }	
        }
      ofile << endl;
    }
  ofile.close();
  cout << "done twophase liquid isobars\n";
}


void TwophaseVaporEnthalpyIsothermsToFile( double tmin = 50., 
                                           double tmax = 1000.,
                                           double dt   = 49.99999)
{
  double            t;
  double            p;
  double            myp;
  double            x;
  double            pmin;
  double            pmax;
  double            dp;
  double            ph2o;
  double            dph2odt;
  Water             water(t,p);
  CriticalPointH2O  cp_h2o;
  TwophaseVapor     twophase_v(t,p,ph2o,dph2odt);
  Brine             brine(t,p,x);
  CriticalCurve     critcurve(t);
  ThreephaseHLV     hlv(t);
  TriplePointNaCl   tp_nacl;
  NaClBoilingCurve  naclboil(t,p);

  ofstream ofile("enthalpy_vl_v_isotherms.dat",ios::out);
  ofile << "vl_v_t\tvl_v_p\tvl_v_wt\tvl_v_h\n";

  for(t = tmin; t <= tmax; t+= dt)
    {
      if(t < cp_h2o.Temperature()) 
        {
          ph2o    = water.SaturationPressure()*1.0e-5;
          dph2odt = 1.0;
          pmax    = ph2o;
        }
      else 
        {
          pmax = critcurve.Pressure();
        }
      if(t > tp_nacl.Temperature())
        pmin = naclboil.Pressure();
      else
        pmin = hlv.Pressure();

      if(t < 400.0) dp = 0.01*(pmax-pmin);
      else dp   = 0.002*(pmax-pmin);

      p    = pmin*1.000000001;
      x    = twophase_v.MassFractionNaCl();
      ofile << t << "\t" << pmin << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;

      for(p = pmin+dp; p < pmax-dp; p+= dp)
        {
          x    = twophase_v.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
        }

      p = pmax;
      if(t < 373.976)
        {
          ofile << t << "\t" << p << "\t" << XNaCl2Weight(0.0) << "\t" << water.VaporSaturationEnthalpyForT() << endl;
        }
      else
        {
          x = critcurve.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
        }
      ofile << endl;
    }
  ofile.close();
  cout << "done twophase vapor isotherms\n";
}


void TwophaseVaporEnthalpyIsobarsToFile( double pmin = 100., 
                                         double pmax = 2200.,
                                         double dp   = 100.)
{
  double            t;
  double            p;
  double            x;
  double            tmin;
  double            tmax;
  double            t_vlh_low;
  double            t_vlh_high;
  double            dt;
  double            ph2o;
  double            dph2odt;
  Water             water(t,p);
  CriticalPointH2O  cp_h2o;
  TwophaseVapor     twophase_v(t,p,ph2o,dph2odt);
  Brine             brine(t,p,x);
  CriticalCurve     critcurve(t);
  ThreephaseHLV     hlv(t);

  ofstream ofile("enthalpy_vl_v_isobars.dat",ios::out);
  ofile << "vl_v_t\tvl_v_p\tvl_v_wt\tvl_v_h\n";

  for(p = pmin; p <= pmax; p += dp)
    {
      t = 1000.;
      if( p > critcurve.Pressure()) continue;
      t = 300.;
	
      if(p < hlv.Pmax())
        {
          t_vlh_low  = hlv.TfromP(p,450.)*0.999999;
          t_vlh_high = hlv.TfromP(p,700.)*1.000001;
      	  
          // low T segment
          if(p < cp_h2o.Pressure()) 
            {
              tmin    = water.SaturationTemperature();
              ofile << tmin << "\t" << p << "\t" << XNaCl2Weight(0.0) << "\t" 
                    << water.VaporSaturationEnthalpyForP() << endl;
            }
          else
            {
              tmin = critcurve.TfromP(p);
              t    = tmin;
              x    = critcurve.MassFractionNaCl();
              ofile << tmin << "\t" << p << "\t" << x*100. << "\t" 
                    << brine.Enthalpy() << endl;
            }
          tmin = double(int(tmin+0.01))+1.0;
          tmax = double(int(t_vlh_low-0.01));
	  
          for(t = tmin+0.01; t <= tmin+0.99; t += 0.01)
            {
              if(p < cp_h2o.Pressure())
                {
                  ph2o    = water.SaturationPressure()*1.0e-5;
                  dph2odt = 1.0;
                }
              x = twophase_v.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
            }
          for(t = tmin+1.0; t <= tmax; t += 1.0)
            {
              if(p < cp_h2o.Pressure())
                {
                  ph2o    = water.SaturationPressure()*1.0e-5;
                  dph2odt = 1.0;
                }
              x = twophase_v.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
            }
          t = t_vlh_low;
          x = twophase_v.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;
          ofile << endl;

          // high T segment
          t = t_vlh_high;
          x = twophase_v.MassFractionNaCl();
          ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;

          tmin = double(int(t_vlh_high+0.01))+1.0;
          for(t = tmin; t <= 1000.; t += 1.0)
            {
              x = twophase_v.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;	      
            }
          ofile << endl;
        }

      else
        {
          tmin = critcurve.TfromP(p);
          t    = tmin;
          x    = critcurve.MassFractionNaCl();
          ofile << tmin << "\t" << p << "\t" << x*100. << "\t" 
                << brine.Enthalpy() << endl;

          tmin = double(int(tmin+0.01))+1.0;
          for(t = tmin; t <= 1000.; t += 1.0)
            {
              x = twophase_v.MassFractionNaCl();
              ofile << t << "\t" << p << "\t" << x*100. << "\t" << brine.Enthalpy() << endl;	      
            }	
        }
      ofile << endl;
    }
  ofile.close();
  cout << "done twophase vapor isobars\n";
}

  
void TwophaseIsoplethsToFile( double wtmin = 5.0, // must be higher than 0 otherwise algorithm is not foolproof
                              double wtmax = 95.0,
                              double dwt   = 5.0)
{
  double t(0.0), tmin(0.0), tmax(0.0), dt(0.0); 
  double p(0.0), pmin(0.0), pmax(0.0), ph2o(1.0), dph2o(1.0);
  double wt(0.0);
  double x(0.0), xl(0.0), xv(0.0), xmax(0.0), xcrit(0.0), xhlv(0.0), diffx(0.0);
  bool   alwaysL(false);
  int    it, ip, ix;

  ThreephaseHLV    hlv(t);
  HaliteLiquidus   liquidus(t,p);
  CriticalCurve    critcurve(t);
  CriticalPointH2O cp_h2o;
  TriplePointNaCl  tp_nacl;
  NaClBoilingCurve naclboil(t,p);
  TwophaseLiquid   twophase_l(t,p,ph2o,dph2o);
  TwophaseVapor    twophase_v(t,p,ph2o,dph2o);
  Water            water(t,p);
  Brine            brine(t,p,x);

  ofstream ofile("enthalpy_vl_isopleths.dat",ios::out);
  ofile << "vl_x_t\tvl_x_p\tvl_x_wt\tvl_x_h\n";    

  for(wt = wtmin; wt <= wtmax; wt += dwt )
    {
      cout << "doing wt = " << wt << endl;
      x     = Weight2XNaCl(wt);
	
      // 1. Determine tmin
      /*
        tmin is
        - either 10C if x < x_liquidus at 10C
        - or something higher if x > x_liquidus at 10C
      */
      t     = 10.0;
      p     = hlv.Pressure();
      xmax  = liquidus.MassFractionNaCl();
      if(x < xmax) tmin = 10.0;
      else
        {
          ix = 0;
          tmin  = 10.0;
          tmax  = tp_nacl.Temperature();
          diffx = 1.0;;
          while( fabs(diffx) > 1.0e-6 )
            {
              ix ++;
              t     = 0.5*(tmin+tmax);
              p     = hlv.Pressure();
              xhlv  = liquidus.MassFractionNaCl();
              diffx = xhlv-x;
              if( fabs(diffx) < 1.0e-6) { tmin = t; break; }
              if( diffx > 0.0 ) tmax = t;
              if( diffx < 0.0 ) tmin = t;
              if(ix > 50)
                {
                  cout << "For wt = " << wt << " no convergence for tmin!\n";
                  cout << "Better terminate. Or enter any character to continue:";
                  char mychar;
                  cin  >> mychar;
                  break;
                }
            }
        }

      // 2. Determine tmax, set dt
      t                   = 1000.;
      xcrit               = critcurve.MassFractionNaCl();
      if(x < xcrit){tmax  = critcurve.TfromX(x); alwaysL = false; }
      else         {tmax  = 1000.1;              alwaysL = true; } // will always be on L side
      dt                  = (tmax - tmin)*0.009999999;
      //cout << "tmin = " << tmin << ", tmax = " << tmax << endl;

      // 3. Get isopleth on L branch
      for(t = tmin; t < tmax; t += dt)
        {
          if(!alwaysL && t > tmax-0.5*dt)
            break; // goes to tmax only for alwaysL==true
          diffx = 1.0;
	    
          if( t <  cp_h2o.Temperature() )
            { 
              pmax = water.SaturationPressure()*1.0e-5;
              ph2o = pmax;
            }
          else                            pmax = critcurve.Pressure();
	    
          if( t <= tp_nacl.Temperature()) pmin = hlv.Pressure();
          else                            pmin = naclboil.Pressure();                    
	    
          it = 0;

          while( fabs(diffx) > 0.0 )
            {
              ++it;
              p     = 0.5*(pmin+pmax);
              xl    = twophase_l.MassFractionNaCl();
              diffx = xl-x;

              //		cout << it << "\t" << diffx << endl;
              if(fabs(diffx) < 1.0e-6)
                {
                  if(t < tp_nacl.Temperature() && pmin < 1.01325)
                    {
                      p = 1.01325;
                      ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                    }
                  else 
                    ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
                  break;
                }
              else
                {
                  if(diffx > 0.0) pmin = p;
                  if(diffx < 0.0) pmax = p;
                }
              if(it > 50)
                {
                  cout << "For t = " << t << " no convergence for p!\n";
                  cout << "Better terminate. Or enter any character to continue:";
                  char mychar;
                  cin  >> mychar;
                  break;
                }
            }
        }

      // 4. ... and now those that also go into the vapor field
      if(!alwaysL) 
        {
          cout << "entering !alwaysL\n";
          t    = tmax;
          p    = critcurve.Pressure();
          ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;

          tmin = tmax;
          tmax = 1000.0;
          dt   = (tmax - tmin)*0.009999999;

          for( t = tmin+dt; t <= tmax; t+= dt )
            {
              p = TwophaseVaporPressureFromXandT( x, t );
              ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
            }
        } // if( !alwaysL )
	
      ofile << endl;
    } // for (wt = )
} // TwophaseIsoplethsToFile



void SinglephaseIsoplethsIsothermsToFile(        double wtmin = 10.0,
                                                 double wtmax = 90.0,
                                                 double dwt   = 9.9999999,
                                                 double tmin  = 50.0,
                                                 double tmax  = 1000.0,
                                                 double dt    = 49.9999999,
                                                 double pmax  = 2200.)
{
  double           wt, x, t, p, pmin, dp;
  CriticalPointH2O cp_h2o;
  TriplePointNaCl  tp_nacl;
  ThreephaseHLV    vlh(t);
  HaliteLiquidus   liquidus(t,p);
  CriticalCurve    critcurve(t);
  Brine            brine(t,p,x);

  char ofilename[60];

  cout << "computing SinglephaseIsoplethsIsothermsToFile\n";
  for(wt = wtmin; wt <= wtmax; wt += dwt*0.9999999)
    {
      sprintf(ofilename,"%s%i%s","enthalpy_1phase_",int(wt+0.001),"wt.dat");
      ofstream ofile(ofilename,ios::out);
      ofile << "vl_x_t\tvl_x_p\tvl_x_wt\tvl_x_h\n";    
      x = Weight2XNaCl(wt);
      cout << "doing wt = " << wt << endl;
      for(t = tmin; t <= tmax; t += dt)
        {
          cout << "t = " << t << endl;
          if( t <= tp_nacl.Temperature())
            {
              p = pmax;
              if(x >= liquidus.MassFractionNaCl()) continue; // shortcut, not the most elegant solution ...
              p = vlh.Pressure();
              if(x >= liquidus.MassFractionNaCl()) continue; // shortcut, not the most elegant solution ...
              else
                {
                  if( t < cp_h2o.Temperature() )        pmin = TwophaseLiquidPressureFromXandT(x,t);
                  else
                    {
                      if( x < critcurve.MassFractionNaCl() ) pmin = TwophaseVaporPressureFromXandT(x, t);
                      else                              pmin = TwophaseLiquidPressureFromXandT(x,t);
                    }
                }
            }
          else pmin = 10.0; // not perfect; 

          dp    = 0.001999999*(pmax- pmin);
          for(p = pmin; p <= pmax; p += dp)
            ofile << t << "\t" << p << "\t" << wt << "\t" << brine.Enthalpy() << endl;
          ofile << endl;
        }
      ofile.close();
    }
}



double TwophaseLiquidPressureFromXandT(double myx, double myt)
{
  double           x(myx), t(myt), p, xl, ph2o(1.0), dph2o(1.0), pmax, pmin, diffx;
  long             ix;
  CriticalPointH2O cp_h2o;
  CriticalCurve    critcurve(t);
  TwophaseLiquid   twophase_l(t,p,ph2o,dph2o);
  ThreephaseHLV    vlh(t);
  TriplePointNaCl  tp_nacl;
  Water            water(t,p);
    
  if( t < cp_h2o.Temperature() )
    {
      // cout << "double TwophaseLiquidPressureFromXandT(double x, double t):\n"
      //      << "Requested for t < Tcrit(H2O). This functionality is not available.\n"
      //      << "T must be >= Tcrit(H2O)\n\n"
      //      << "... Terminating ...\n";
      // exit(0);

      ph2o  = water.SaturationPressure()*1.0e-5;
      pmax  = ph2o;
      pmin  = vlh.Pressure();
      diffx = 1.0; 
      ix    = 0;

      while( fabs(diffx) > 0.0 )
        {
          ix++;
          p     = 0.5*(pmin+pmax);
          xl    = twophase_l.MassFractionNaCl();
          diffx = xl-x;
          //cout << ix << "\t" << p << "\t" << xl << "\t" << diffx << endl;
	    
          if(fabs(diffx) < 1.0e-8)
            {
              break;
            }
          else
            {
              if(diffx < 0.0) pmax = p;
              if(diffx > 0.0) pmin = p;
            }
          if(ix > 50)
            {
              cout << "double TwophaseLiquidPressureFromXandT(double x, double t):\n"
                   << "low T Iteration for Pressure failed\n"
                   << "for x = " << x << "and t = " << t << endl
                   << "... Terminating ...\n";
              exit(0);
            }
        }
      return p;
    }



  else
    {
      if( x < critcurve.MassFractionNaCl() )
        {
          cout << "double TwophaseLiquidPressureFromXandT(double x, double t):\n"
               << "Requested for x < xcrit(T). This is physically impossible.\n"
               << "x is " << x << ", and xcrit is " << critcurve.MassFractionNaCl() << endl
               << "x must be >= xcrit(T)\n\n"
               << "... Terminating ...\n";
          exit(0);
        }
	
      if( t < tp_nacl.Temperature() ) pmin = vlh.Pressure();
      else                            pmin  = 10.0; //arbitrary, not failsafe!
	
      pmax  = critcurve.Pressure();
      diffx = 1.0;
      ix    = 0;
      // cout << "pmin = " << pmin << endl;
      // cout << "pmax = " << pmax << endl;
	
      while( fabs(diffx) > 0.0 )
        {
          ix++;
          p     = 0.5*(pmin+pmax);
          xl    = twophase_l.MassFractionNaCl();
          diffx = xl-x;
          //cout << ix << "\t" << p << "\t" << xl << "\t" << diffx << endl;
	    
          if(fabs(diffx) < 1.0e-8)
            {
              break;
            }
          else
            {
              if(diffx < 0.0) pmax = p;
              if(diffx > 0.0) pmin = p;
            }
          if(ix > 50)
            {
              cout << "double TwophaseLiquidPressureFromXandT(double x, double t):\n"
                   << "high-T Iteration for Pressure failed\n\n"
                   << "... Terminating ...\n";
              exit(0);
            }
        }
      return p;
    }
}


double TwophaseVaporPressureFromXandT(double myx, double myt)
{
  double           x(myx), t(myt), p, xv;
    
  CriticalPointH2O cp_h2o;
  CriticalCurve    critcurve(t);
  TwophaseVapor    twophase_v(t,p,1.0,1.0);
  ThreephaseHLV    vlh(t);
  TriplePointNaCl  tp_nacl;
    
  if( t < cp_h2o.Temperature() )
    {
      cout << "double TwophaseVaporPressureFromXandT(double x, double t):\n"
           << "Requested for t < Tcrit(H2O). This functionality is not available.\n"
           << "T must be >= Tcrit(H2O)\n\n"
           << "... Terminating ...\n";
      exit(0);
    }

  if( x >= critcurve.MassFractionNaCl() )
    {
      cout << "double TwophaseVaporPressureFromXandT(double x, double t):\n"
           << "Requested for x > xcrit(T). This is physically impossible.\n"
           << "x is " << x << ", and xcrit is " << critcurve.MassFractionNaCl() << endl
           << "x must be <= xcrit(T)\n\n"
           << "... Terminating ...\n";
      exit(0);
    }
      
  double pmin;
  if( t < tp_nacl.Temperature() ) pmin = vlh.Pressure();
  else                            pmin  = 10.0; //arbitrary, not failsafe!

  double pmax  = critcurve.Pressure();
  double diffx = 1.0;
  long   ix    = 0;
  // cout << "pmin = " << pmin << endl;
  // cout << "pmax = " << pmax << endl;

  while( fabs(diffx) > 0.0 )
    {
      ix++;
      p     = 0.5*(pmin+pmax);
      xv    = twophase_v.MassFractionNaCl();
      diffx = xv-x;
      //	cout << ix << "\t" << p << "\t" << xv << "\t" << diffx << endl;

      if(fabs(diffx) < 1.0e-8)
        {
          break;
        }
      else
        {
          if(diffx > 0.0) pmax = p;
          if(diffx < 0.0) pmin = p;
        }
      if(ix > 50)
        {
          cout << "double TwophaseVaporPressureFromXandT(double x, double t):\n"
               << "Iteration for Pressure failed\n\n"
               << "... Terminating ...\n";
          exit(0);
        }
    }
  return p;
}

double LiquidusTemperatureFromXandP(const double& myx, const double& myp)
{
  double t,x;
  HaliteLiquidus liquidus(t,myp);
  NaClMeltingCurve naclmelt(t,myp);

  const double tmelt = naclmelt.TmeltFromP();
  double tmax = tmelt;
  double tmin = 10.;

  double resid = 1.;
  while(fabs(resid) > 1.0e-6)
    {
      t = 0.5*(tmin+tmax);
      x = liquidus.MassFractionNaCl();
      resid = x-myx;
      if( resid > 0.0) tmax = t;
      else if( resid < 0.0 ) tmin = t;
      else break;
    }
  return t;
}

double LiquidusPressureFromXandT(double myx, double myt, double mypmax)
{
  // This function assumes that xsat at pmax is larger than xsat at p_vlh 
  double           p, x(myx), t(myt), xsat, pmax, pmin;
  bool             x_gt_xsat_highp, x_lt_xsat_lowp;
  HaliteLiquidus   liquidus(t,p);
  ThreephaseHLV    vlh(t);

  p               = mypmax;
  cout << "xsat high p is " << liquidus.MassFractionNaCl() << endl;
  x_gt_xsat_highp = (x > liquidus.MassFractionNaCl());
  pmax            = p;

  p               = vlh.Pressure();
  cout << "xsat low p is " << liquidus.MassFractionNaCl() << endl;
  x_lt_xsat_lowp  = (x < liquidus.MassFractionNaCl());
  pmin            = p;

  if( (x_gt_xsat_highp && x_lt_xsat_lowp) 
      ||
      (!x_gt_xsat_highp && !x_lt_xsat_lowp)
      )
    {
      double diffx(1.0);
      long   ix(0);
        
      while(fabs(diffx) > 1.0e-10)
        {
          ++ix;
          p     = 0.5*(pmin+pmax);
          xsat  = liquidus.MassFractionNaCl();
          diffx = xsat - x;
          //  cout << ix << "\t" << p << "\t" << xsat << "\t" << diffx << endl;
            
          if( fabs(diffx) < 1.0e-10)
            {
              return p;
            }
          else
            {
              if(      (diffx > 0.0) && (liquidus.DCompositionDP() < 0.0) ) pmin = p;
              else if( (diffx < 0.0) && (liquidus.DCompositionDP() < 0.0) ) pmax = p;
              else if( (diffx > 0.0) && (liquidus.DCompositionDP() > 0.0) ) pmax = p;
              else if( (diffx < 0.0) && (liquidus.DCompositionDP() > 0.0) ) pmin = p;
              else
                {
                  cout << "double LiquidusPressureFromXandT(double myx, double myt, double mypmax):\n"
                       << "couldn't find proper conditions for iteration\n"
                       << "... Terminating ...\n";
                  exit(0);
                }
            }
          if(ix > 60)
            {
              cout << "double LiquidusPressureFromXandT(double myx, double myt, double mypmax):\n"
                   << "Iteration for Pressure failed\n\n"
                   << "... Terminating ...\n";
              exit(0);
            }
        }
    }
  else
    {
      cout << "double LiquidusPressureFromXandT(double myx, double myt, double mypmax):\n"
           << "Liquidus is not intercepted by isopleth x = " << x << " at t = " << t << endl
           << "Returning 0.0\n\n";
      return 0.0;
    }
}
  
double GetDP( const double& p )
{
  if( p > 700.e5 ) return 100.e5;
  else if ( p > 500.e5 ) return 25.e5;
  else if ( p > 400.e5 ) return 10.e5;
  else return 5.e5;
}

// untested, unadapted
// void IsenthalpsInTPX( const double hmin = 0.5e6,
//                       const double hmax = 3.5e6,
//                       const double dh   = 0.5e6,
//                       const double pmin = 5.,
//                       const double pmax = 2200.,
//                             double dp   = 100.)
// {
//   ofstream ofile("test.dat");
//   // First Determine a few characteristic pressures that determine th phase didgarm topology in isobaric sections
//   double              t = 1000.;

//   double              p = pmax;
//   double              x = 0.0;
//   Brine               brine(t,p,x);
//   NaClMeltingCurve    naclmelt(t,p);
//   HaliteLiquidus      liquidus(t,p);
//   CriticalCurve       critcurve(t);
//   CriticalPointH2O    cp_h2o;
//   ThreephaseHLV       vlh(t);
//   double              wt,tmax, tmin, resid, myh, tsat, wtsat;
//   bool                first;
//   int                 count;

//   double    pcrit1000 = critcurve.Pressure();
//   cout << pcrit1000 << endl; char mychar; cin >> mychar;
//   double    pvlhmax   = vlh.Pmax();
//   double    pcrith2o  = cp_h2o.Pressure();


//   for( double h = hmin; h < hmax+0.5*dh; h += dh )
//     {
//       cout << "doing h = " << h << endl;
//       double dp = GetDP(pmax);
//       for( p = pmax; p > pmin; p -= dp )
//         {
//           cout << "doing p = " << p << endl;
//           dp = GetDP(p);

//           if( p >= pcrit1000 ) // only remaining phase diagram element is the liquidus
//             {
//               cout << "entered if1\n";
//               // first store nacl melt enthalpy data
//               double tnaclmelt = naclmelt.TmeltFromP();
//               t                = tnaclmelt;
//               double hnaclmelt = brine.Enthalpy();

//               //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!... possibly non-unique, same enthalpy may be hit twice by liquidus !!!
//               if( hnaclmelt > h )
//                 {
//                   tmax = tnaclmelt;
//                   tmin = 10;
//                   resid = 1.0;
//                   while( fabs(resid) > 1.0e-3 )
//                     {
//                       t     = 0.5*(tmin+tmax);
//                       x     = liquidus.MassFractionNaCl();
//                       myh   = brine.Enthalpy();
//                       resid = myh - h;
//                       if(resid > 0.)       tmax = t;
//                       else if(resid < 0.)  tmin = t;
//                       else break;
//                     }
//                   tsat  = t;
//                   wtsat = XNaCl2Weight( x );
//                 }

//               // then iterate along isopleths
//               first = false;
//               for(wt = 0.0; wt <= 100.; wt += 5.)
//                 {
//                   cout << "doing wt = " << wt << endl;
//                  x = Weight2XNaCl(wt); // needed for Brine object
//                   t = 10.;
//                   double xsat10 = liquidus.MassFractionNaCl();
//                   if(XNaCl2Weight(xsat10) >= wt) // we can iterate over the whole t range without intersecting liquidus
//                     {
//                       tmax      = 1000.;
//                       tmin      = 10.;
//                       resid     = 1.e5;
//                       bool converged = false;
//                       count = 0;
//                       while(fabs(resid) > 1.0e-3)
//                         {
//                           ++count;
//                           if( count > 100 ) break;
//                           t     = 0.5*(tmin+tmax);
//                           myh   = brine.Enthalpy();
//                           resid = h-myh;
//                           if( fabs(resid) < 1.0e-3 ) converged = true;
//                           if(      resid > 0.0 ) tmin = t;
//                           else if( resid < 0.0 ) tmax = t;
//                           else break;
//                         }
//                       if( converged ) ofile << t << "\t" << p << "\t" << wt << "\t" << h << endl; // should be converged in any case but testing
//                     }
//                   else
//                     {
//                       t   = LiquidusTemperatureFromXandP(Weight2XNaCl(wt),p);
//                       myh = brine.Enthalpy();
//                       if(myh > h)
//                         {
//                           if(first == false){
//                             first = true;
//                             ofile << tsat << "\t" << p << "\t" << wtsat << "\t" << h << endl;
//                             continue; // surface would be in LH region
//                           }
//                           else continue;
//                         }
//                       else
//                         {
//                           tmin = t;
//                           tmax = 1000.;
//                           resid     = 1.e5;
//                           bool converged = false;
//                           count = 0;
//                           while(fabs(resid) > 1.0e-3)
//                             {
//                               ++count;
//                               if( count > 100 ) break;
//                               t     = 0.5*(tmin+tmax);
//                               myh   = brine.Enthalpy();
//                               resid = h-myh;
//                               if( fabs(resid) < 1.0e-3 ) converged = true;
//                               if(      resid > 0.0 ) tmin = t;
//                               else if( resid < 0.0 ) tmax = t;
//                               else break;
//                             }
//                           if( converged ) ofile << t << "\t" << p << "\t" << wt << "\t" << h << endl;
//                         }
//                     }
//                 }
//             }
//           else if( p >= pvlhmax ) // only VL and LH twophase regions
//             {
//               // determine temperature on critical curve
//               double crit  = critcurve.TfromP(p);
//               t      = tcrit;
//               xcrit  = critcurve.MassFractionNaCl();
//               wtcrit = XNaCl2Weight(xcrit);

//               for(wt = 0.0; wt <= 100.; wt += 5.)
//                 {
//                   cout << "doing wt = " << wt << endl;
//                   x = Weight2XNaCl(wt); // needed for Brine object
//                   tmin = 10.;
//                   tmax = 1000.;
//                   bool converged = false;
//                   resid = 1.0;
//                   while( fabs(resid) > 1.0e-3 )
//                     {
//                       t   = 0.5*(tmin+tmax);
//                       myh = CalcEnthalpyAt_T_and_X(t,p,x); 

//                     }


//                 }
//             }
//           else if( p > pcrith2o )
//             {}
//           else
//             {}
//         }
//       ofile << endl;
//     }

// }




} // namespace csp
