#ifndef TDLIB_H
#define TDLIB_H

#include <iostream>

using namespace std;

inline double GetTempInC(){
    double t;
    cout << "Please enter T in C   : ";
    cin  >> t;
    return t;
}

inline double GetTempInK(){
    double t;
    cout << "Please enter T in KELVIN : ";
    cin  >> t;
    return t;
}

inline double GetTempMinInC(){
    double t;
    cout << "Please enter Tmin in C   : ";
    cin  >> t;
    return t;
}

inline double GetTempMaxInC(){
    double t;
    cout << "Please enter Tmax in C   : ";
    cin  >> t;
    return t;
}

inline double GetTempIncrement(){
    double ti;
    cout << "Please enter Temperature Increment in C   : ";
    cin  >> ti;
    return ti;
}

inline double GetPressInBar(){
    double p;
    cout << "Please enter p in bar : ";
    cin  >> p;
    return p;
}
inline double GetMaxPressInBar(){
    double p;
    cout << "Please enter max p in bar : ";
    cin  >> p;
    return p;
}
inline double GetMinPressInBar(){
    double p;
    cout << "Please enter min p in bar : ";
    cin  >> p;
    return p;
}

inline double GetPressIncrement(){
    double pi;
    cout << "Please enter p increment in bar : ";
    cin  >> pi;
    return pi;
}

inline double GetDvDx(){
    double dvdx;
    cout << "Please enter dvdx     : ";
    cin  >> dvdx;
    return dvdx;
}

inline double GetDpDx(){
    double dpdx;
    cout << "Please enter dpdx (with exp)    : ";
    cin  >> dpdx;
    return dpdx;
}

inline double GetX(){
    double x;
    cout << "Please enter x     : ";
    cin  >> x;
    return x;
}

inline double GetWt(){
    double x;
    cout << "Please enter weight percent     : ";
    cin  >> x;
    return x;
}

inline double GetMolal(){
    double x;
    cout << "Please enter molal              : ";
    cin  >> x;
    return x;
}



#endif
